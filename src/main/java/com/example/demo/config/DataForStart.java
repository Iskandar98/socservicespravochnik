package com.example.demo.config;

import com.example.demo.entities.*;
import com.example.demo.repositories.*;
import com.example.demo.service.FilesStorageService;
import com.example.demo.service.StorageService;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Builder
@Component
public class DataForStart implements CommandLineRunner {
    @Autowired
    public RegionDataRepo regionDataRepo;

    @Autowired
    public OblastDataRepo oblastDataRepo;

    @Autowired
    public  Organization_group_Repo organization_group_repo;
    @Autowired
    public  OrganizationsRepo organizationsRepo;

    @Autowired
    public ChildrenRepo childrenRepo;

    @Autowired
    public ApplicantsRepo applicantsRepo;

    @Autowired
    public PrivelegesRepo privelegesRepo;

    @Resource
    FilesStorageService storageService;


    @Override
    public void run(String... args) throws Exception {

        storageService.deletAll();
        storageService.init();
//        Children  children =Children.builder()
//                .id(1)
//                .name("Антон")
//                .surname("Антонов")
//                .middlename("Антонович")
//                .build();
//        childrenRepo.save(children);
//
//        Applicants applicants = Applicants.builder()
//                .id(1)
//                .name("Sergey")
//                .surname("Sergeyeev")
//                .middle_name("Nikolaevich")
//                .build();
//        applicantsRepo.save(applicants);
//
//        Priveleges priveleges = Priveleges.builder()
//                .id(1)
//                .build();
//        privelegesRepo.save(priveleges);

        Oblast obl0 =new Oblast(1," Баткенская область");
        Oblast obl1 =new Oblast(2,"Джалал-Абадская область");
        Oblast obl2 =new Oblast(3,"Нарынская область");
        Oblast obl3 =new Oblast(4,"Ошская область");
        Oblast obl4 =new Oblast(5,"Таласская область");
        Oblast obl5 =new Oblast(6,"Чуйская область");
        Oblast obl6 =new Oblast(7,"Иссык-Кульская область");
        Oblast obl7 =new Oblast(8,"город Бишкек");
        Oblast obl8 =new Oblast(9,"город Ош");

        oblastDataRepo.save(obl0);
        oblastDataRepo.save(obl1);
        oblastDataRepo.save(obl2);
        oblastDataRepo.save(obl3);
        oblastDataRepo.save(obl4);
        oblastDataRepo.save(obl5);
        oblastDataRepo.save(obl6);
        oblastDataRepo.save(obl7);
        oblastDataRepo.save(obl8);
        Region region1=new Region(1,"Баткенская район",obl0 );
        Region region2=new Region(2,"Кадамжайский район",obl0 );
        Region region3=new Region(3,"Лейлекский район",obl0 );
        regionDataRepo.save(region1);
        regionDataRepo.save(region2);
        regionDataRepo.save(region3);
        Region region4=new Region(4,"Аксыйский район",obl1 );
        Region region5=new Region(5,"Ала-Букинский район",obl1 );
        Region region6=new Region(6,"Базар-Коргонский район",obl1 );
        Region region7=new Region(7,"Ноокенский район",obl1 );
        Region region8=new Region(8,"Сузакский район",obl1 );
        Region region9=new Region(9,"Тогуз-Тороуский район",obl1 );
        Region region10=new Region(10,"Токтогульский район",obl1 );
        Region region11=new Region(11,"Чаткальский район",obl1 );
        regionDataRepo.save(region4);
        regionDataRepo.save(region5);
        regionDataRepo.save(region6);
        regionDataRepo.save(region7);
        regionDataRepo.save(region8);
        regionDataRepo.save(region9);
        regionDataRepo.save(region10);
        regionDataRepo.save(region11);
        Region region12=new Region(12,"Ак-Суйский район",obl6 );
        Region region13=new Region(13,"Жети-Огузский район",obl6 );
        Region region14=new Region(14,"Иссык-Кульский район",obl6 );
        Region region15=new Region(15,"Тонский район",obl6 );
        Region region16=new Region(16,"Тюпский район",obl6 );
        regionDataRepo.save(region12);
        regionDataRepo.save(region13);
        regionDataRepo.save(region14);
        regionDataRepo.save(region15);
        regionDataRepo.save(region16);
        Region region17 = new Region(17,"Тюпский район",obl2);
        Region region18 = new Region(18,"Ак-Талинский район",obl2);
        Region region19 = new Region(19,"Ат-Башинский",obl2);
        Region region20 = new Region(20,"Жумгальский район",obl2);
        Region region21 = new Region(21,"Кочкорский ",obl2);
        Region region22 = new Region(22,"Нарынский район",obl2);
        regionDataRepo.save(region17);
        regionDataRepo.save(region18);
        regionDataRepo.save(region19);
        regionDataRepo.save(region20);
        regionDataRepo.save(region21);
        regionDataRepo.save(region22);
        Region region23 = new Region(23,"Алайский район",obl3);
        Region region24 = new Region(24,"Араванский район",obl3);
        Region region25 = new Region(25,"Кара-Кульжидский район",obl3);
        Region region26 = new Region(26,"Кара-Сууйский район",obl3);
        Region region27 = new Region(27,"Ноокатский район",obl3);
        Region region28 = new Region(28,"Узгенский район",obl3);
        Region region29 = new Region(29,"Чон-Алайский район",obl3);
        regionDataRepo.save(region23);
        regionDataRepo.save(region24);
        regionDataRepo.save(region25);
        regionDataRepo.save(region26);
        regionDataRepo.save(region27);
        regionDataRepo.save(region28);
        regionDataRepo.save(region29);
        Region region30 = new Region(30,"Бакай-Атинский район",obl4);
        Region region31 = new Region(31,"Кара-Бууринский район",obl4);
        Region region32 = new Region(32,"Манасский район",obl4);
        Region region33 = new Region(33,"Талаский район",obl4);
        regionDataRepo.save(region30);
        regionDataRepo.save(region31);
        regionDataRepo.save(region32);
        regionDataRepo.save(region33);
        Region region34 = new Region(34,"Аламудинский район",obl5);
        Region region35 = new Region(35,"Жайылский район",obl5);
        Region region36 = new Region(36,"Кеминский район",obl5);
        Region region37 = new Region(37,"Московский район",obl5);
        Region region38 = new Region(38,"Панфиловский район",obl5);
        Region region39 = new Region(39,"Сокулукский район",obl5);
        Region region40 = new Region(40,"Чуйский район",obl5);
        Region region41 = new Region(41,"Ыссык-Атинский район",obl5);
        regionDataRepo.save(region34);
        regionDataRepo.save(region35);
        regionDataRepo.save(region36);
        regionDataRepo.save(region37);
        regionDataRepo.save(region38);
        regionDataRepo.save(region39);
        regionDataRepo.save(region40);
        regionDataRepo.save(region41);


    Organizations org1 = new Organizations(1,"kolobok");
        Organizations org2 = new Organizations(2,"jdcnkjnc");
    organizationsRepo.save(org1);
        organizationsRepo.save(org2);


    Organization_group orgGroup1 = new Organization_group(1,org1,"1-group");
    Organization_group orgGroup2 = new Organization_group(1,org1,"2-group");
    Organization_group orgGroup3 = new Organization_group(1,org1,"3-group");

        Organization_group orgGroup4 = new Organization_group(2,org1,"1-group");
        Organization_group orgGroup5 = new Organization_group(2,org1,"2-group");
        Organization_group orgGroup6 = new Organization_group(2,org1,"3-group");

        organization_group_repo.save(orgGroup1);
        organization_group_repo.save(orgGroup2);
        organization_group_repo.save(orgGroup3);
}}
