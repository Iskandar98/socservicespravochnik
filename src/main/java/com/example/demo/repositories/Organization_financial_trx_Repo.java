package com.example.demo.repositories;

import com.example.demo.entities.Organization_financial_trx;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Organization_financial_trx_Repo extends JpaRepository<Organization_financial_trx,Integer> {
}
