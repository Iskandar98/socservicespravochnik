package com.example.demo.repositories;

import com.example.demo.entities.Applicants;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplicantsRepo extends JpaRepository<Applicants,Integer> {
//    List<Applicants> getByEmail();
}
