package com.example.demo.repositories;

import com.example.demo.entities.Organizations;
import com.example.demo.entities.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrganizationsRepo  extends JpaRepository<Organizations,Integer> {

}
