package com.example.demo.controller;

import com.example.demo.entities.Applicants;
import com.example.demo.repositories.ApplicantsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.security.PublicKey;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/applicants")
public class ApplicantsController {
    @Autowired
    public ApplicantsRepo applicantsRepo;

    @RequestMapping("/")
    public List<Applicants> getAllApplicants(){
        return applicantsRepo.findAll();
    }

    @PostMapping("/")
    public void addApplicants(@RequestBody Applicants applicants){
    applicantsRepo.save(applicants);
    }

    @PutMapping
    public  Applicants updateApplicants(@RequestBody Applicants newapplicants,@PathVariable("id") Integer id){
        return applicantsRepo.findById(id).map(applicants -> {
            applicants.setName(newapplicants.getName());
            applicants.setSurname(newapplicants.getSurname());
            applicants.setMiddle_name(newapplicants.getMiddle_name());
            applicants.setPassport_range(newapplicants.getPassport_range());
            applicants.setPassport_id(newapplicants.getPassport_id());
            applicants.setPasssport_issuer(newapplicants.getPasssport_issuer());
            applicants.setPassport_issue_date(newapplicants.getPassport_issue_date());
            applicants.setJob_position(newapplicants.getJob_position());
            applicants.setWork_office_address(newapplicants.getWork_office_address());
            applicants.setPhone(newapplicants.getPhone());
            applicants.setEmail(newapplicants.getEmail());
            applicants.setExtra_phone(newapplicants.getExtra_phone());
            applicants.setAddress(newapplicants.getAddress());
            applicants.setRegistration_date(newapplicants.getRegistration_date());
            applicants.setUsername(newapplicants.getUsername());
            applicants.setPassword(newapplicants.getPassword());
            applicants.setRaw_password(newapplicants.getRaw_password());
            applicants.setPassport_file_front(newapplicants.getPassport_file_front());
            applicants.setPassport_file_back(newapplicants.getPassport_file_back());
            return applicantsRepo.save(applicants);
        }).orElseGet(()->
        {newapplicants.setId(id);
        return  applicantsRepo.save(newapplicants);
        });
    }
    @GetMapping("/{id}")
    public Optional<Applicants> getIdApplicant(@PathVariable("id") Integer id){
        return applicantsRepo.findById(id);
    }

    @DeleteMapping("/{id}")
    public  String deletById(@PathVariable("id")Integer id){
        applicantsRepo.deleteById(id);
        return "Deleted";
    }
}
