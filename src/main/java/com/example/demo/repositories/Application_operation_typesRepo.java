package com.example.demo.repositories;

import com.example.demo.entities.Application_deduct_types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Application_operation_typesRepo extends JpaRepository<Application_deduct_types,Integer> {
}
