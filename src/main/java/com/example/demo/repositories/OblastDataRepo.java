package com.example.demo.repositories;

import com.example.demo.entities.Oblast;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OblastDataRepo extends JpaRepository<Oblast, Integer> {

}
