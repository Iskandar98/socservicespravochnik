package com.example.demo.repositories;

import com.example.demo.entities.Management_users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Management_users_Repo  extends JpaRepository<Management_users,Integer> {
}
