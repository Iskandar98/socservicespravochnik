package com.example.demo.repositories;

import com.example.demo.entities.Age_group_types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Age_group_typesRepo extends JpaRepository<Age_group_types,Integer> {
}
