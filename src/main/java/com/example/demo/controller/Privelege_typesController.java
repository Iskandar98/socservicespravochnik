package com.example.demo.controller;

import com.example.demo.entities.Privelege_types;
import com.example.demo.repositories.Privelege_typesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/privelege_type")
public class Privelege_typesController {
    @Autowired
    public Privelege_typesRepo privelege_typesRepo;

    @RequestMapping("/")
    public List<Privelege_types> getAllPrivelegType(){
        return  privelege_typesRepo.findAll();
    }

    @PostMapping("/")
    public  void addPrivelegeType(@RequestBody Privelege_types newprivelegetypes){
        privelege_typesRepo.save(newprivelegetypes);
    }

    @PutMapping("/{id}")
    public  Privelege_types updatePrivelegeType(@RequestBody Privelege_types newprivelegetype,@PathVariable Integer id){
        return privelege_typesRepo.findById(id).map(privelege_types -> {
           privelege_types.setName(newprivelegetype.getName());
           privelege_types.setPriority(newprivelegetype.getPriority());
           privelege_types.setDescr(newprivelegetype.getDescr());
           privelege_types.setFor_disability(newprivelegetype.getFor_disability());
            return privelege_typesRepo.save(privelege_types);
        }).orElseGet(()->{
            newprivelegetype.setId(id);
            return privelege_typesRepo.save(newprivelegetype);
        });
    }

    @GetMapping("/{id}")
    public Optional<Privelege_types> getIdPrivelgeType(@PathVariable("id") Integer id ){
        return privelege_typesRepo.findById(id);
    }

    @DeleteMapping ("/{id}")
    public String deleteById(@PathVariable("id") Integer id ){
        privelege_typesRepo.deleteById(id);
        return "Deleted";
    }
}
