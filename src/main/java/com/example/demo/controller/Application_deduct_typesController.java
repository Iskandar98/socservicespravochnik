package com.example.demo.controller;

import com.example.demo.entities.Application_deduct_types;
import com.example.demo.repositories.Application_deduct_typesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/Application_deduct_typesController")

public class Application_deduct_typesController {
    @Autowired
    public Application_deduct_typesRepo application_deduct_typesRepo;

    @RequestMapping("/")
    public List<Application_deduct_types> getAllApplication_deduct_types(){
        return application_deduct_typesRepo.findAll();
    }

    @PostMapping("/add")
    public void application_deduct_types(@RequestBody Application_deduct_types newApplication_deduct_types){
        application_deduct_typesRepo.save(newApplication_deduct_types);
    }

    @PutMapping("/update/{id}")
    public Application_deduct_types updateApplication_reject_types(@RequestBody Application_deduct_types newApplication_deduct_types, @PathVariable Integer id){
        return application_deduct_typesRepo.findById(id).map(application_deduct_types -> {
            application_deduct_types.setDescr(newApplication_deduct_types.getDescr());
            application_deduct_types.setName(newApplication_deduct_types.getName());
            return application_deduct_typesRepo.save(newApplication_deduct_types);
        }).orElseGet(()->{newApplication_deduct_types.setId(id);
            return application_deduct_typesRepo.save(newApplication_deduct_types);
        });

    }

    @GetMapping("/get/obl/{id}")
    public Optional<Application_deduct_types> getIdApplication_deduct_types(@PathVariable("id") Integer id){
        return  application_deduct_typesRepo.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deletById(@PathVariable("id") Integer id){
        application_deduct_typesRepo.deleteById(id);
        return "Deleted";
    }

}