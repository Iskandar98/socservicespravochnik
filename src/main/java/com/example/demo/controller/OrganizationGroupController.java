package com.example.demo.controller;

import com.example.demo.entities.Organization_group;
import com.example.demo.repositories.Organization_group_Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/organizationGroup")
public class OrganizationGroupController {


    @Autowired
    public Organization_group_Repo organization_group_repo;

    @RequestMapping("/")
    public List<Organization_group> getAllOrganGroup(){
        return organization_group_repo.findAll();
    }

    @PostMapping("/")
    public void  addOrganGroup(@RequestBody Organization_group newOrganGroup){
        organization_group_repo.save(newOrganGroup);
    }

    @PutMapping("/{id}")
    public Organization_group updateOrganGroup(@RequestBody Organization_group newOrganGroup,@PathVariable Integer id){
      return  organization_group_repo.findById(id).map(organization_group -> {
          organization_group.setOrganizations(newOrganGroup.getOrganizations());
          organization_group.setName(newOrganGroup.getName());
//          organization_group.setCapacity(newOrganGroup.getCapacity());
//          organization_group.setLanguage_id(newOrganGroup.getLanguage_id());
      return organization_group_repo.save(newOrganGroup);
      }).orElseGet(()->{
          newOrganGroup.setId(id);

          return  organization_group_repo.save(newOrganGroup);
      });
    }

    @GetMapping("/id/{organization_id}")
    public Optional<Organization_group> getOrganGroup(@PathVariable("organization_id") Integer id){
        return  organization_group_repo.findById(id);
    }


    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable("id") Integer id){
        organization_group_repo.deleteById(id);
        return "Deleted";
    }
}
