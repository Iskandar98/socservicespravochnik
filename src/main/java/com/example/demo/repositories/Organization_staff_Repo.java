package com.example.demo.repositories;

import com.example.demo.entities.Organization_staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Organization_staff_Repo extends JpaRepository<Organization_staff,Integer> {
}
