package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.type.NumericBooleanType;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "age_group_types")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Age_group_types {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    @Column(name = "name")
    Character name;
    @Column(name = "min_age")
    NumericBooleanType min_age;
    @Column(name = "max_age")
    NumericBooleanType max_age;
    @Column(name = "max_num_by_law")
    short max_num_by_law;
    @Column(name = "enable_min_age")
    NumericBooleanType enable_min_age;
    @Column(name = "enable_max_age")
    NumericBooleanType enable_max_age;
}
