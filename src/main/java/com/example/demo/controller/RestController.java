package com.example.demo.controller;

import com.example.demo.entities.Oblast;
import com.example.demo.entities.Region;
import com.example.demo.repositories.OblastDataRepo;
import com.example.demo.repositories.RegionDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin
@RequestMapping("/test")
public class RestController {

@Autowired
public RegionDataRepo regionDataRepo;

@Autowired
public OblastDataRepo oblastDataRepo;


@GetMapping("/getallRegion")
public List<Region> getAllRegion(){
    return regionDataRepo.findAll();
}

@GetMapping("/getallOblast")
public List<Oblast> getAllOblast(){

    return oblastDataRepo.findAll();
}

@PostMapping("/add")
public void addOblData(@RequestBody Oblast oblData){
    oblastDataRepo.save(oblData);
}
@PutMapping("/put/{id}")
public Oblast updateOblast(@RequestBody Oblast newOblast, @PathVariable Integer id){
 return oblastDataRepo.findById(id).map(oblast -> {
     oblast.setOblast(newOblast.getOblast());
     return oblastDataRepo.save(oblast);
 }).orElseGet(()->{
    newOblast.setId(id);
    return oblastDataRepo.save(newOblast);
 });


}
@GetMapping("/get/obl/{id}")
public Optional<Oblast> getIdObl(@PathVariable("id") Integer id){
    return oblastDataRepo.findById(id);
    //return oblastDataRepo.getOne(id);
}
    @GetMapping("/get/reg/{id}")
    public Optional<Region> getIdReg(@PathVariable("id") Integer id){
        return regionDataRepo.findById(id);
        //return oblastDataRepo.getOne(id);
    }

@GetMapping("/getalloblastbyidRegion/{id}")
public List<Region> getAllOblastByIdRegion(@PathVariable("id") Integer id){
        return regionDataRepo.getAllRayonByOblastId(id);
        //return oblastDataRepo.getOne(id);
    }
@DeleteMapping("/delete/{id}")
public String deleteByID(@PathVariable("id") Integer id){
    oblastDataRepo.deleteById(id);
    return "Deleted";
}





}
