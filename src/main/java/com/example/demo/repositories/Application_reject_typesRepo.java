package com.example.demo.repositories;

import com.example.demo.entities.Application_reject_types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Application_reject_typesRepo extends JpaRepository<Application_reject_types,Integer> {
}
