package com.example.demo.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.Optional;

public interface StorageService {
    void init();
    void store(MultipartFile file);

    Path load(String fileName);

    Resource loadAsResource(String filename);
    void deleteAll();

    Optional<Object> loadAll();
}
