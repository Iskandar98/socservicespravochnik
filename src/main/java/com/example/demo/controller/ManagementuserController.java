package com.example.demo.controller;


import com.example.demo.entities.Management_users;
import com.example.demo.entities.Organization_group;
import com.example.demo.repositories.Management_users_Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/managementUser")
public class ManagementuserController {

    @Autowired
    public Management_users_Repo management_users_repo;

    @RequestMapping("/")
    public List<Management_users> getAllManagUser(){
        return management_users_repo.findAll();
    }

    @PostMapping("/")
    public void  addManagUser(@RequestBody Management_users newManagUser){
        management_users_repo.save(newManagUser);
    }

    @PutMapping("/{id}")
    public Management_users updateManagUser(@RequestBody Management_users newManagUser,@PathVariable Integer id){
        return  management_users_repo.findById(id).map(management_users -> {
            management_users.setUsername(newManagUser.getUsername());
            management_users.setPassword(newManagUser.getPassword());
            management_users.setRow_password(newManagUser.getRow_password());
            management_users.setEmail(newManagUser.getEmail());
            management_users.setPhone(newManagUser.getPhone());
            management_users.setFull_name(newManagUser.getFull_name());
            management_users.setRegistration_date(newManagUser.getRegistration_date());
            management_users.setLast_login_date(newManagUser.getLast_login_date());

            return management_users_repo.save(newManagUser);
        }).orElseGet(()->{
            newManagUser.setId(id);

            return  management_users_repo.save(newManagUser);
        });
    }

//    @GetMapping("/{id}")
//    public Optional<Management_users> getIdManagUser(@PathVariable("id") Integer id){
//        return  management_users_repo.findById(id);
//    }

    @GetMapping("/{id}")
    public String deleteById(@PathVariable("id") Integer id){
        management_users_repo.deleteById(id);
        return "Deleted";
    }
}
