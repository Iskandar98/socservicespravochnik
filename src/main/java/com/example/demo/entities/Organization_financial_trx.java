package com.example.demo.entities;

import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.type.TimeZoneType;
import org.hibernate.type.descriptor.sql.RealTypeDescriptor;
import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;
import org.w3c.dom.Text;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "organization_financial_trx")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Organization_financial_trx implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;


    @ManyToOne(optional = false)
    @JoinColumn(name = "organization_id")
    Organizations organizations  ;


    @Column(name =  "type_id")
    SmallIntTypeDescriptor type_id;

    @Column(name =  "registration_date")
    TimeZoneType registration_date;

    @Column(name =  "amount")
    RealTypeDescriptor amount;


}

