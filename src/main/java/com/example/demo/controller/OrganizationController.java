package com.example.demo.controller;

import com.example.demo.entities.Organizations;
import com.example.demo.repositories.OrganizationsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/orgamization")
public class OrganizationController {
    @Autowired
    public OrganizationsRepo organizationsRepo;

    @GetMapping("/")
    public List<Organizations> getAllOrganizs(){
        return organizationsRepo.findAll();
    }
    @PostMapping("/")
    public  void  addOrganizs(@RequestBody Organizations organizations){
        organizationsRepo.save(organizations);
    }
    @PutMapping("/{id}")
    public Organizations updateOrganizs(@RequestBody Organizations newOrganizs,@PathVariable Integer id){
        return organizationsRepo.findById(id).map(organizations -> {
//            organizations.setOrganizations(newOrganizs.getOrganizations());
            organizations.setName(newOrganizs.getName());
//            organizations.setAddress(newOrganizs.getAddress());
//            organizations.setState_regions(newOrganizs.getState_regions());
//            organizations.setPhones(newOrganizs.getPhones());
//            organizations.setType_id(newOrganizs.getType_id());
//            organizations.setOrganization_purpose_id(newOrganizs.getOrganization_purpose_id());
//            organizations.setBuild_year(newOrganizs.getBuild_year());
//            organizations.setTin(newOrganizs.getTin());
//            organizations.setOkpo(newOrganizs.getOkpo());
//            organizations.setGked(newOrganizs.getGked());
//            organizations.setFloors(newOrganizs.getFloors());
//            organizations.setArea(newOrganizs.getArea());
//            organizations.setLat(newOrganizs.getLat());
//            organizations.setLng(newOrganizs.getLng());
//            organizations.setAvailable(newOrganizs.getAvailable());
//            organizations.setPostal_code(newOrganizs.getPostal_code());
//            organizations.setEmail(newOrganizs.getEmail());
//            organizations.setSсhedule_type(newOrganizs.getSсhedule_type());
//            organizations.setWork_duration_type(newOrganizs.getWork_duration_type());
//            organizations.setSewerage(newOrganizs.getSewerage());
//            organizations.setHot_water_supply(newOrganizs.getHot_water_supply());
//            organizations.setCold_water_supply(newOrganizs.getCold_water_supply());
//            organizations.setRestroom(newOrganizs.getRestroom());
//            organizations.setSpecial_restroom(newOrganizs.getSpecial_restroom());
//            organizations.setDrinking_water(newOrganizs.getDrinking_water());
//            organizations.setWashstands(organizations.getWashstands());
//            organizations.setCentral_heating(organizations.getCentral_heating());
//            organizations.setHot_meals(newOrganizs.getHot_meals());
//            organizations.setFire_alarm(newOrganizs.getFire_alarm());
//            organizations.setRamps(newOrganizs.getRamps());
//            organizations.setElevators(newOrganizs.getElevators());
//            organizations.setMedical_staff(newOrganizs.getMedical_staff());
//            organizations.setSpecial_staff(newOrganizs.getSpecial_staff());
//            organizations.setAccess_roads(newOrganizs.getAccess_roads());
//            organizations.setMedical_office(newOrganizs.getMedical_office());
//            organizations.setDefectologist_office(newOrganizs.getDefectologist_office());
//            organizations.setPsychologist_office(newOrganizs.getPsychologist_office());
//            organizations.setAdapted_education_programs(newOrganizs.getAdapted_education_programs());
//            organizations.setSpecial_trainer_count(newOrganizs.getSpecial_trainer_count());
//            organizations.setFirst_floor_rooms(newOrganizs.getFirst_floor_rooms());
//            organizations.setSpeech_therapist_office(newOrganizs.getSpeech_therapist_office());
//            organizations.setWellness_service(newOrganizs.getWellness_service());
//            organizations.setMusical_class(newOrganizs.getMusical_class());
//            organizations.setDefectologist_class(newOrganizs.getDefectologist_class());
//            organizations.setForeign_language_class(newOrganizs.getForeign_language_class());
//            organizations.setPsychological_service(newOrganizs.getPsychological_service());
//            organizations.setComputer_games(newOrganizs.getComputer_games());
//            organizations.setOther_service(newOrganizs.getOther_service());
            return organizationsRepo.save(organizations);
        }).orElseGet(()->{
            newOrganizs.setId(id);
            return organizationsRepo.save(newOrganizs);
        });
    }

//    @GetMapping("/{id}")
//    public Optional<Organizations> getBytId(@PathVariable("id") Integer id){
//        return organizationsRepo.findById(id);
//    }


    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable("id") Integer id){
        organizationsRepo.deleteById(id);
        return "Deleted";
    }


    @GetMapping("/{id}")
    public  String listByReg(@PathVariable("id") Integer id){
    return "";
    }


}
