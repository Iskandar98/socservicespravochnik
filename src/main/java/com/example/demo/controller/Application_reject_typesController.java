package com.example.demo.controller;

import com.example.demo.entities.Application_reject_types;
import com.example.demo.repositories.Application_reject_typesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/application_reject_typesController")

public class Application_reject_typesController {
    @Autowired
    public Application_reject_typesRepo application_reject_typesRepo;

    @RequestMapping("/")
    public List<Application_reject_types> getAllApplication_reject_types(){
        return application_reject_typesRepo.findAll();
    }

    @PostMapping("/add")
    public void addApplication_reject_types(@RequestBody Application_reject_types newApplication_reject_types){
        application_reject_typesRepo.save(newApplication_reject_types);
    }

    @PutMapping("/update/{id}")
    public Application_reject_types updateApplication_reject_types(@RequestBody Application_reject_types newApplication_reject_types, @PathVariable Integer id){
        return application_reject_typesRepo.findById(id).map(application_reject_types -> {
            application_reject_types.setName(newApplication_reject_types.getName());
            return application_reject_typesRepo.save(newApplication_reject_types);
        }).orElseGet(()->{newApplication_reject_types.setId(id);
            return application_reject_typesRepo.save(newApplication_reject_types);
        });

    }

    @GetMapping("/get/obl/{id}")
    public Optional<Application_reject_types> getIdApplication_reject_types(@PathVariable("id") Integer id){
        return  application_reject_typesRepo.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deletById(@PathVariable("id") Integer id){
        application_reject_typesRepo.deleteById(id);
        return "Deleted";
    }

}