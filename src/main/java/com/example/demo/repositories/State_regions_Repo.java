package com.example.demo.repositories;

import com.example.demo.entities.State_regions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface State_regions_Repo extends JpaRepository<State_regions,Integer> {
}
