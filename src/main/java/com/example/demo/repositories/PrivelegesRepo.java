package com.example.demo.repositories;

import com.example.demo.entities.Priveleges;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrivelegesRepo extends JpaRepository<Priveleges, Integer> {
}
