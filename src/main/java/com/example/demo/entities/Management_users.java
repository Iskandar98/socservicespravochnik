package com.example.demo.entities;


import com.example.demo.config.DataForStart;
        import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
        import lombok.*;
        import lombok.experimental.FieldDefaults;
        import org.hibernate.type.TimeZoneType;
        import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;
        import org.w3c.dom.Text;

        import javax.persistence.*;
        import java.io.Serializable;
        import java.math.BigInteger;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "management_users")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Management_users implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "username")
    Character username  ;

    @Column(name =  "password")
    Character password;


    @Column(name = "row_password")
    Character row_password  ;

    @Column(name = "email")
    Character email  ;

    @Column(name = "phone")
    Character phone  ;

    @Column(name = "full_name")
    Character  full_name  ;

    @Column(name = "registration_date")
    TimeZoneType registration_date  ;

    @Column(name = "last_login_date")
    TimeZoneType  last_login_date  ;


    @ManyToOne(optional = false)
    @JoinColumn(name = "region_id")
    State_regions state_regions ;
}

