package com.example.demo.repositories;


import com.example.demo.entities.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface RegionDataRepo extends JpaRepository<Region, Integer> {
    List<Region> getAllRayonByOblastId(Integer id);
 

}
