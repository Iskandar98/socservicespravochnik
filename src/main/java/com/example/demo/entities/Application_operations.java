package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonLocation;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "application_operations")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Application_operations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "application_id")
   Children children;


    @ManyToOne(optional = false)
    @JoinColumn(name = "manager_id")
    Management_users management_users;


    @ManyToOne(optional = false)
    @JoinColumn(name = "operation_type_id")
    Application_operation_types application_operation_types;

    @Column(name = "prev_date")
    JsonLocation prev_date;
    @Column(name = "actual_date")
    JsonLocation actual_date;
    @Column(name = "registration_date")
    Timestamp registration_date;
    @Column(name = "order_file")
    Character order_file;

}
