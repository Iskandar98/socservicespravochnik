package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "children")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Children {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    @Column(name = "name")
    String name;
    @Column(name = "surname")
    String surname;
    @Column(name = "middlename")
    String middlename;
    @Column(name = "sex")
    boolean sex;
    @Column(name = "birth_date")
    Timestamp birth_date;
    @Column(name = "bith_certificate_range")
    Timestamp bith_certificate_range;
    @Column(name = "bith_certificate_issue_date")
    Timestamp bith_certificate_issue_date;
    @Column(name = "registration_date")
    Timestamp registration_date;

    @ManyToOne(optional = false)
    @JoinColumn(name = "organization_id")
    Organizations organizations ;


    @ManyToOne(optional = false)
    @JoinColumn(name = "status_id")
    Children_status_history children_status_history;


    @ManyToOne(optional = false)
    @JoinColumn(name = "organization_group_id")
    Organization_group organization_group;

    @Column(name = "bith_certificate_issue")
    Character bith_certificate_issue;
    @Column(name = "bith_certificate_file")
    Character bith_certificate_file;
    @Column(name = "expiration_date")
    Timestamp expiration_date;

    @ManyToOne(optional = false)
    @JoinColumn(name = "region_id")
    State_regions state_regions;




}
