package com.example.demo.controller;


import com.example.demo.entities.Organization_domain;
import com.example.demo.entities.Organizations;
import com.example.demo.repositories.Organization_domain_Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/orgamization_domains")
public class Organization_domains_Controller {
    @Autowired
    public Organization_domain_Repo organization_domain_repo;
    @GetMapping("/")
    public List<Organization_domain> getAllOrganizaition_domains(){
        return organization_domain_repo.findAll();
    }
    @PostMapping("/")
    public  void  addOrganization_domains(@RequestBody Organization_domain newOrganization_domain){
        organization_domain_repo.save(newOrganization_domain);
    }
    @PutMapping("/{id}")
    public Organization_domain updateOrganization_domains(@RequestBody Organization_domain newOrganization_domain,@PathVariable Integer id){
        return organization_domain_repo.findById(id).map(organization_domain -> {
            organization_domain.setName(newOrganization_domain.getName());
            organization_domain.setData_type(newOrganization_domain.getData_type());


            return organization_domain_repo.save(organization_domain);
        }).orElseGet(()->{
            newOrganization_domain.setId(id);
            return organization_domain_repo.save(newOrganization_domain);
        });
    }
    @GetMapping("/{id}")
    public Optional<Organization_domain> getIdOrganization_domains(@PathVariable("id") Integer id){
        return organization_domain_repo.findById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable("id") Integer id){
        organization_domain_repo.deleteById(id);
        return "Deleted";
    }

}

