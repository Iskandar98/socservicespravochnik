package com.example.demo.entities;



import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.type.TimeZoneType;
import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;
import org.w3c.dom.Text;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Children_status_history")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Children_status_history implements Serializable {


    @Id


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "status")
    String status  ;

    @Column(name =  "registration_date")
    TimeZoneType registration_date;



//    @Column(name = "notice")
//    Text notice  ;


    @ManyToOne(optional = false)
    @JoinColumn(name = "child_id")
   Children children;

    @ManyToOne(optional = false)
    @JoinColumn(name =  "reject_type_id")
    Application_reject_types application_reject_types;

    @Column(name = "document_file")
    Character  document_file  ;

    @Column(name = "document_additional_file")
    Character document_additional_file  ;

    @ManyToOne(optional = false)
    @JoinColumn(name = "deduct_type_id")
    Application_deduct_types application_deduct_types  ;
}
