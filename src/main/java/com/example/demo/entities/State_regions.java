package com.example.demo.entities;



import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "State_regions")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class State_regions implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "name")
    String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "parent_id")
    State_regions state_regions;

    @Column(name = "parent_name")
    String  parent_name;

    @Column(name = "enabled")
    Boolean enabled;



//    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.PERSIST})
//    @JoinColumn(name = "id_region", nullable = false)
//    private Region region;


}