package com.example.demo.repositories;

import com.example.demo.entities.Children_status_history;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Children_status_history_Repo extends JpaRepository<Children_status_history, Integer> {

}
