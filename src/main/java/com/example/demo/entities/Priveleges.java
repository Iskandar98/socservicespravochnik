package com.example.demo.entities;



import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Priveleges")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Priveleges implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "licence_id")
    String licence_id;

    @Column(name = "licence_issue_date")
    Timestamp licence_issue_date;

    @Column(name = "licence_file")
    String licence_file;

    @ManyToOne(optional = false)
    @JoinColumn(name = "privelege_type_id")
    Privelege_types privelege_types;

    @ManyToOne(optional = false)
    @JoinColumn(name = "child_id")
    Children children;

    @Column(name = "disability_privelege_type_id")
    Integer disability_privelege_type_id;

    @Column(name = "state_disability_exists")
    Boolean state_disability_exists;

    @Column(name = "state_allowance_exists")
    Boolean state_allowance_exists;



//    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.PERSIST})
//    @JoinColumn(name = "id_region", nullable = false)
//    private Region region;


}