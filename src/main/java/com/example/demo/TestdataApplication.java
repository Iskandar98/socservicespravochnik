package com.example.demo;

import com.example.demo.service.FilesStorageService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
public class TestdataApplication {
	@Resource
	FilesStorageService storageService;
	public static void main(String[] args) {
		SpringApplication.run(TestdataApplication.class, args);
	}




}
