package com.example.demo.entities;

import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.type.TimeZoneType;
import org.hibernate.type.descriptor.sql.RealTypeDescriptor;
import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;
import org.w3c.dom.Text;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Organization_staff")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Organization_staff implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "full_name")
    Character full_name  ;

    @Column(name =  "job_position")
    Character job_position;

    @Column(name =  "phone")
    Character phone;

    @Column(name =  "work_starting_date")
    TimeZoneType work_starting_date;

    @Column(name =  "work_ending_date")
    TimeZoneType work_ending_date;

    @Column(name =  "registration_date")
    TimeZoneType registration_date;


    @ManyToOne(optional = false)
    @JoinColumn(name ="organization_id")
    Organizations organizations;

    @Column(name =  "edu_level_id")
    SmallIntTypeDescriptor edu_level_id;

    @Column(name =  "sex")
    SmallIntTypeDescriptor sex;

    @Column(name =  "works_from")
    Date works_from;

    @Column(name =  "job_position_id")
    SmallIntTypeDescriptor job_position_id;


}

