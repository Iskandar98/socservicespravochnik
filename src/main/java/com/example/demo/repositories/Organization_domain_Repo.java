package com.example.demo.repositories;

import com.example.demo.entities.Organization_domain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Organization_domain_Repo extends JpaRepository<Organization_domain, Integer> {
}
