package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Timestamp;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "applicants")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class  Applicants {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    @Column(name = "name")
    String name;
    @Column(name = "surname")
    String surname;
    @Column(name = "middle_name")
    String middle_name;
    @Column(name = "passport_range")
    Character passport_range;
    @Column(name = "passport_id")
    Character passport_id;
    @Column(name = "passsport_issuer")
    Character passsport_issuer;
    @Column(name = "passport_issue_date")
    Timestamp passport_issue_date;
    @Column(name = "job_position")
    Character job_position;
    @Column(name = "work_office_address")
    Character work_office_address;
    @Column(name = "phone")
    Character phone;
    @Column(name = " email")
    Character email;
    @Column(name = "extra_phone")
    Character extra_phone;
    @Column(name = "address")
    Character address;
    @Column(name = "registration_date")
    Timestamp registration_date;
    @Column(name = " username")
    Character username;
    @Column(name = "password")
    Character password;
    @Column(name = " raw_password")
    Character raw_password;
    @Column(name = "passport_file_front")
    Character passport_file_front;
    @Column(name = "passport_file_back")
    Character passport_file_back;
}
