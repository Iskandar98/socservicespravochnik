package com.example.demo.controller;

import com.example.demo.entities.Users;
import com.example.demo.repositories.Users_Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/usersController")

    public class UsersController {
        @Autowired
        public Users_Repo users_repo;

        @RequestMapping("/")
        public List<Users> getAllUsers(){
            return users_repo.findAll();
        }

        @PostMapping("/add")
        public void addUsers(@RequestBody Users newUsers){
            users_repo.save(newUsers);
        }

        @PutMapping("/update/{id}")
        public Users updateUsers(@RequestBody Users newUsers, @PathVariable Integer id){
            return users_repo.findById(id).map(users -> {
            users.setEmail(newUsers.getEmail());
            users.setFull_name(newUsers.getFull_name());
            users.setJob_position(newUsers.getJob_position());
            users.setLast_login_date(newUsers.getLast_login_date());
            users.setPassword(newUsers.getPassword());
            users.setPhone(newUsers.getPhone());
            users.setRaw_password(newUsers.getRaw_password());
            users.setRegistration_date(newUsers.getRegistration_date());
            users.setUsername(newUsers.getUsername());
            users.setOrganizations(newUsers.getOrganizations());
            return users_repo.save(newUsers);
        }).orElseGet(()->{newUsers.setId(id);
                return users_repo.save(newUsers);
            });

        }

    @GetMapping("/get/{id}")
    public Optional<Users> getIdUsers(@PathVariable("id") Integer id){
        return  users_repo.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deletById(@PathVariable("id") Integer id){
        users_repo.deleteById(id);
        return "Deleted";
    }

}

