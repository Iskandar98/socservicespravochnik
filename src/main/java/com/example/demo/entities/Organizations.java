package com.example.demo.entities;


import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Organizations")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Organizations implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "name")
    String name;

//    @Column(name = "address")
//    String address;
//
//    @Column(name = "capacity")
//    Integer capacity;
//
//    @ManyToOne(optional = false)
//    @JoinColumn(name = "region_id")
//    State_regions state_regions;
//
//    @Column(name = "phones")
//    String phones;
//
//    @Column(name = "type_id")
//    SmallIntTypeDescriptor type_id;
//
//    @Column(name = "organization_purpose_id")
//    SmallIntTypeDescriptor organization_purpose_id;
//
//    @Column(name = "organization_type_id")
//    SmallIntTypeDescriptor organizations;
//
//    @Column(name = "build_year")
//    Integer build_year;
//
//    @Column(name = "tin")
//    String tin;
//
//    @Column(name = "okpo")
//    String okpo;
//
//    @Column(name = "gked")
//    String gked;
//
//    @Column(name = "floors")
//    SmallIntTypeDescriptor floors;
//
//    @Column(name = "area")
//    Integer area;
//
////    @ManyToOne(optional = false)
////    @JoinColumn(name = "condition_id")
////    Organization_domain organization_domain;
//
//    @Column(name = "lat")
//    String lat;
//
//    @Column(name = "lng")
//    String lng;
//
//    @Column(name = "available")
//    Boolean available;
//
//    @Column(name = "postal_code")
//    String postal_code;
//
//    @Column(name = "email")
//    String email;
//
//    @Column(name = "sсhedule_type")
//    SmallIntTypeDescriptor sсhedule_type;
//
//    @Column(name = "work_duration_type")
//    SmallIntTypeDescriptor work_duration_type;
//
//    @Column(name = "sewerage")
//    Boolean sewerage;
//
//    @Column(name = "hot_water_supply")
//    Boolean hot_water_supply;
//
//    @Column(name = "cold_water_supply")
//    Boolean cold_water_supply;
//
//    @Column(name = "restroom")
//    Boolean restroom;
//
//    @Column(name = "special_restroom")
//    Boolean special_restroom;
//
//    @Column(name = "drinking_water")
//    Boolean drinking_water;
//
//    @Column(name = "washstands")
//    Boolean washstands;
//
//    @Column(name = "central_heating")
//    Boolean central_heating;
//
//    @Column(name = "hot_meals")
//    Boolean hot_meals;
//
//    @Column(name = "fire_alarm")
//    Boolean fire_alarm;
//
//    @Column(name = "ramps")
//    Boolean ramps;
//
//    @Column(name = "elevators")
//    Boolean elevators;
//
//    @Column(name = "medical_staff")
//    Boolean medical_staff;
//
//    @Column(name = "special_staff")
//    Boolean special_staff;
//
//    @Column(name = "access_roads")
//    Boolean access_roads;
//
//    @Column(name = "medical_office")
//    Boolean medical_office;
//
//    @Column(name = "defectologist_office")
//    Boolean defectologist_office;
//
//    @Column(name = "psychologist_office")
//    Boolean psychologist_office;
//
//    @Column(name = "adapted_education_programs")
//    Boolean adapted_education_programs;
//
//    @Column(name = "special_trainer_count")
//    SmallIntTypeDescriptor special_trainer_count;
//
//    @Column(name = "first_floor_rooms")
//    Boolean first_floor_rooms;
//
//    @Column(name = "speech_therapist_office")
//    Boolean speech_therapist_office;
//
//    @Column(name = "wellness_service")
//    Boolean wellness_service;
//
//    @Column(name = "musical_class")
//    Boolean musical_class;
//
//    @Column(name = "defectologist_class")
//    Boolean defectologist_class;
//
//    @Column(name = "speech_therapist_class")
//    Boolean speech_therapist_class;
//
//    @Column(name = "foreign_language_class")
//    Boolean foreign_language_class;
//
//    @Column(name = "psychological_service")
//    Boolean psychological_service;
//
//    @Column(name = "computer_games")
//    Boolean computer_games;
//
//    @Column(name = "other_service")
//    Boolean other_service;
//
//    @ManyToOne(optional = false)
//    @JoinColumn(name = "condition_id")
//    Organization_domain organization_domain ;
////    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.PERSIST})
////    @JoinColumn(name = "id_region", nullable = false)
////    private Region region;
//
// String listprage;
}
