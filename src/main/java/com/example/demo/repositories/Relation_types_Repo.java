package com.example.demo.repositories;

import com.example.demo.entities.Relation_types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Relation_types_Repo extends JpaRepository<Relation_types,Integer> {
}
