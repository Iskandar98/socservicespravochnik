package com.example.demo.repositories;

import com.example.demo.entities.Privelege_types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Privelege_typesRepo extends JpaRepository<Privelege_types,Integer> {
}
