package com.example.demo.repositories;

import com.example.demo.entities.Organization_job_positions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Organization_job_positions_Repo extends JpaRepository<Organization_job_positions,Integer> {
}
