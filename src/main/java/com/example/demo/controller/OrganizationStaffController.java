package com.example.demo.controller;

import com.example.demo.entities.Organization_staff;
import com.example.demo.repositories.Organization_staff_Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/organizationStaff")
public class OrganizationStaffController {


  @Autowired
  public Organization_staff_Repo organization_staff_repo;

  @RequestMapping("/")
  public List<Organization_staff> getAllOrganStaff(){
      return  organization_staff_repo.findAll();
  }

  @PostMapping("/")
  public void addOrganStaff(@RequestBody Organization_staff newOrganStaff){
      organization_staff_repo.save(newOrganStaff);
  }

  @PutMapping("/{id}")
   public Organization_staff updateOrganStaff(@RequestBody Organization_staff newOrganStaff,@PathVariable Integer id){
      return organization_staff_repo.findById(id).map(organization_staff -> {
          organization_staff.setOrganizations(newOrganStaff.getOrganizations());
          organization_staff.setFull_name(newOrganStaff.getFull_name());
          organization_staff.setJob_position(newOrganStaff.getJob_position());
          organization_staff.setPhone(newOrganStaff.getPhone());
          organization_staff.setWork_starting_date(newOrganStaff.getWork_starting_date());
          organization_staff.setWork_ending_date(newOrganStaff.getWork_ending_date());
          organization_staff.setRegistration_date(newOrganStaff.getRegistration_date());
          organization_staff.setEdu_level_id(newOrganStaff.getEdu_level_id());
          organization_staff.setSex(newOrganStaff.getSex());
          organization_staff.setWorks_from(newOrganStaff.getWorks_from());
          organization_staff.setJob_position(newOrganStaff.getJob_position());
          return organization_staff_repo.save(organization_staff);
      }).orElseGet(()->{
          newOrganStaff.setId(id);
          return organization_staff_repo.save(newOrganStaff);
      });
  }

  @GetMapping("/{id}")
  public Optional<Organization_staff> getIdOrganStaff(@PathVariable("id") Integer id){
      return organization_staff_repo.findById(id);
  }
  @DeleteMapping("/{id}")
  public String deleteById(@PathVariable("id") Integer id){

      organization_staff_repo.deleteById(id);
      return "Deleted";
  }


}
