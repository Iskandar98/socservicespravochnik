package com.example.demo.controller;


import com.example.demo.entities.Age_group_types;
import com.example.demo.repositories.Age_group_typesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping("/age_group_types")
public class AgeGroupTypesController {
    @Autowired
    public Age_group_typesRepo age_group_typesRepo;

    @RequestMapping("/")
     public List<Age_group_types> getAllAgeGroupType(){
        return age_group_typesRepo.findAll();
    }

    @PostMapping("/")
    public void addAgeGroupType(@RequestBody Age_group_types newAgeGroupType){
        age_group_typesRepo.save(newAgeGroupType);
    }

    @PutMapping("/{id}")
    public Age_group_types updateAgeGroupType(@RequestBody Age_group_types newAgeGroupType,@PathVariable Integer id){
        return age_group_typesRepo.findById(id).map(age_group_types -> {
            age_group_types.setName(newAgeGroupType.getName());
            age_group_types.setMin_age(newAgeGroupType.getMin_age());
            age_group_types.setMax_age(newAgeGroupType.getMax_age());
            age_group_types.setMax_num_by_law(newAgeGroupType.getMax_num_by_law());
            age_group_types.setEnable_max_age(newAgeGroupType.getEnable_max_age());
            age_group_types.setEnable_min_age(newAgeGroupType.getEnable_min_age());
            return age_group_typesRepo.save(newAgeGroupType);
        }).orElseGet(()->{newAgeGroupType.setId(id);
        return age_group_typesRepo.save(newAgeGroupType);
        });

    }

    @GetMapping("/{id}")
    public Optional<Age_group_types> getIdAgeGroupType(@PathVariable("id") Integer id){
        return  age_group_typesRepo.findById(id);
    }

    @DeleteMapping("/{id}")
    public String deletById(@PathVariable("id") Integer id){
        age_group_typesRepo.deleteById(id);
        return "Deleted";
    }

}
