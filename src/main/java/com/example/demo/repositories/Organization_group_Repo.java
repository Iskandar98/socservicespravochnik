package com.example.demo.repositories;

import com.example.demo.entities.Organization_group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Organization_group_Repo extends JpaRepository<Organization_group,Integer> {
}
