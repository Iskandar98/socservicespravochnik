package com.example.demo.repositories;

import com.example.demo.entities.Children;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChildrenRepo extends JpaRepository<Children,Integer> {
//  List<Children> findByName();
}
