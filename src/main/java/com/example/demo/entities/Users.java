package com.example.demo.entities;



import com.example.demo.config.DataForStart;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Users")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Users implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "username")
    String username;

    @Column(name = "password")
    String password;

    @Column(name = "raw_password")
    String raw_password;

    @Column(name = "email")
    String email;

    @Column(name = "phone")
    String phone;

    @Column(name = "full_name")
    String full_name;

    @Column(name = "job_position")
    String job_position;

    @ManyToOne(optional = false)
    @JoinColumn(name = "organization_id")
    Organizations organizations;

    @Column(name = "registration_date")
    Timestamp registration_date;

    @Column(name = "last_login_date")
    Timestamp last_login_date;


//    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.PERSIST})
//    @JoinColumn(name = "id_region", nullable = false)
//    private Region region;


}